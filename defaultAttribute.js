/* 
* Test if first attribute is assumed when passed in.
*/
var util = require('util');
//var test = require('nodeunit');

function doSomething (options) {
    console.log('Options: ' + util.inspect(options));

    //test.equal(this.options.cwd, '/test', 'Property should default to parent string?');
    console.log('Property should default to parent string: /test?');
    console.log('this.options.cwd: ' + options.cwd);
}

doSomething('/test');