/*
* Create a simple web server for testing.
*/

var http = require('http');

http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'textplain' });
    res.end('Welcome to the nodejs playground!');
}).listen(1337, '127.0.0.1');

console.log('Server running at http://127.0.0.1:1337/');