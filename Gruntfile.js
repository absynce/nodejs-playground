'use strict';

module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        // Configuration to be run (and then tested).
        launch: {
            info: {
                options: {
                    remote: '?',
                    remotepath: '~/',
                    sitePath: '/var/node-launch-test'
                }
            },
            removeOldTempDir: true,
            createTempDir: true,
            checkout: true,
            installDependencies: true,
            createVersionedDir: true,
            moveTempToVersioned: true,
            symbolicLink: true
        },
        ignite: {
            start: true,
            restart: true,
            stop: true
        },
        release: {
            options: {
                npm: false
            }
        }
            
    });

    grunt.loadNpmTasks('grunt-release');
    grunt.loadNpmTasks('grunt-launch');

    grunt.registerTask('deploy', ['launch', 'ignite:restart']);

    // Start, stop or restart the app using forever
    grunt.registerMultiTask('ignite', function () {
        var done  = this.async();
        var share = global.launchConfig;

        // TODO: Split action.js into another npm module so it can be utilized here.
        var cmd   = ('forever ' + this.target + ' ' + share.info.livePath + '/server.js').split(' ');
        var pname = cmd.shift();
        var proc  = require('child_process').spawn(pname, cmd);
        grunt.log.writeln(('\n  $ ' + pname + ' ' + cmd.join(' ')).blue);
        process.stdout.write('\n    ');

        proc.stdout.on('data', function (data) {
            process.stdout.write(('' + data).replace(/\n/g, '\n    ').grey);
        });

        proc.stderr.on('data', function (data) {
            process.stdout.write(('' + data).replace(/\n/g, '\n    ').red);
        });

        proc.on('exit', function (code) {
            if (code === 0) {
                grunt.log.ok('App successfully restarted.');
                done();
            }
            else {
                grunt.log.error('Failed to restart app.');

                done(false);
            }
        });

        proc.stdin.end();
    });

};